use rayon::iter::{IndexedParallelIterator, IntoParallelRefIterator, IntoParallelRefMutIterator,
                  ParallelIterator};

use crate::light::colour::colourConstants::{S0DATA, S1DATA, S2DATA};

// we will make this a constant when const fn is stabilised
//pub const COMPUTED_ILLUMINANT_DATA: [f64; 81] = computedIlluminantData();

pub fn computedIlluminantData() -> [f64; 81] {
    let mut computedIlluminantList: [f64; 81] = [0.0; 81];

    const COLOUR_TEMP_KELVIN: f64 = 6504.0;
    const XD: f64 = (-4607000000.0 / (COLOUR_TEMP_KELVIN * COLOUR_TEMP_KELVIN * COLOUR_TEMP_KELVIN) ) + (2967800.0 / (COLOUR_TEMP_KELVIN * COLOUR_TEMP_KELVIN)) + (99.11 / COLOUR_TEMP_KELVIN) + 0.244063;
    const YD: f64 = -3.0 * (XD * XD) + 2.87 * XD - 0.275;
    const M1: f64 = (-1.3515 - 1.7703 * XD + 5.9114 * YD) / (0.0241 + 0.2562 * XD - 0.7341 * YD);
    const M2: f64 = (0.03 - 31.4424 * XD + 30.0717 * YD) / (0.0241 + 0.2562 * XD - 0.7341 * YD);

    // change the above constants (XD, YD, M1, M2) to variables as below when const fn is stablised

    //let XD: f64 = (-4607000000.0 / COLOUR_TEMP_KELVIN.powi(3)) + (2967800.0 / COLOUR_TEMP_KELVIN.powi(2)) + (99.11 / COLOUR_TEMP_KELVIN) + 0.244063;
    //let YD: f64 = -3.0 * xd.powi(2) + 2.87 * xd - 0.275;
    //let M1: f64 = (-1.3515 - 1.7703 * xd + 5.9114 * yd) / (0.0241 + 0.2562 * xd - 0.7341 * yd);
    //let M2: f64 = (0.03 - 31.4424 * xd + 30.0717 * yd) / (0.0241+0.2562*xd-0.7341 * yd);

    // index is wavelengths of light, stepped by 5, from 380 to 780

    computedIlluminantList
        .par_iter_mut()
        .zip(S0DATA.par_iter())
        .zip(S1DATA.par_iter())
        .zip(S2DATA.par_iter())
        .for_each(|(((illuminantList, s0), s1), s2)| {
            *illuminantList = COLOUR_TEMP_KELVIN * s0 + M1 * s1 + M2 * s2;
        });


    computedIlluminantList
}