use rayon::iter::{IndexedParallelIterator, IntoParallelRefIterator, IntoParallelRefMutIterator,
                  ParallelIterator};

use gdk::RGBA;
use crate::light::colour::computedIlluminantData::computedIlluminantData;
use crate::light::colour::colourConstants::{X2DATA, Y2DATA, Z2DATA};
use crate::light::eigenVectors::eigenVectorConstants::EIGENVECTOR_AVERAGE_DATA;
use crate::structs::generalStructs::{LAB, XYZ};

pub fn reconstructedTransmissionData(glassDiameter: f64, beerSRM: f64) -> [f64; 81] {
    let mut reconstructedTransmissionList: [f64; 81] = [0.0; 81];

    // index is wavelengths of light, stepped by 5, from 380 to 780
    reconstructedTransmissionList
        .par_iter_mut()
        .zip(EIGENVECTOR_AVERAGE_DATA.par_iter())
        .for_each(|(transmission, eigenvectorData)| {
            *transmission = eigenvectorData.powf(glassDiameter).powf(beerSRM / 12.7);
        });

    reconstructedTransmissionList
}

pub fn twoArraySum(firstArray: [f64; 81], secondArray: [f64; 81]) -> f64 {
    // this does the weird spreadsheet thing of (array1[0] * array2[0]) + (array1[0] * array2[0]) ...
    firstArray
        .par_iter()
        .zip(secondArray.par_iter())
        .map(|(first, second)|{
            first * second
        }).sum()
}

pub fn threeArraySum(firstArray: [f64; 81], secondArray: [f64; 81], thirdArray: [f64; 81]) -> f64 {
    // this does the weird spreadsheet thing of (array1[0] * array2[0]) + (array1[0] * array2[0]) ...
    firstArray
        .par_iter()
        .zip(secondArray.par_iter())
        .zip(thirdArray.par_iter())
        .map(|((first, second), third)|{
            first * second * third
        }).sum()
}

pub fn singleMCU(volumeInGallons: f64, weightInPounds: f64, grainLVB: f64) -> f64 {
    (grainLVB * weightInPounds) / volumeInGallons
}

pub fn beerSRM(totalMCU: f64) -> f64 {
    1.4922 * totalMCU.powf(0.6859)
}

pub fn grainSRMToLAB(glassDiameter: f64, beerSRM: f64) -> LAB {
    // computedIlluminantData will be replaced with the constant COMPUTED_ILLUMINANT_DATA
    let computedIlluminantData: [f64; 81] = computedIlluminantData();

    let reconstructedTransmissionData: [f64; 81] = reconstructedTransmissionData(glassDiameter, beerSRM);

    let randomAssEquation: f64 = 100.0 / twoArraySum(computedIlluminantData, Y2DATA);
    let Y2: f64 = randomAssEquation * threeArraySum(reconstructedTransmissionData, Y2DATA, computedIlluminantData);
    let X2: f64 = randomAssEquation * threeArraySum(reconstructedTransmissionData, X2DATA, computedIlluminantData);
    let Z2: f64 = randomAssEquation * threeArraySum(reconstructedTransmissionData, Z2DATA, computedIlluminantData);
    let whitePointXr2: f64 = twoArraySum(computedIlluminantData, X2DATA) * randomAssEquation;
    let whitePointYr2: f64 = twoArraySum(computedIlluminantData, Y2DATA) * randomAssEquation;
    let whitePointZr2: f64  = twoArraySum(computedIlluminantData, Z2DATA) * randomAssEquation;

    let fY: f64 = if (Y2 / whitePointYr2) > 0.008856 {
        (Y2 / whitePointYr2).powf(1.0/3.0)
    } else {
        7.787 * (Y2 / whitePointYr2) + 16.0/116.0
    };

    let fZ: f64 = if (Z2 / whitePointZr2) > 0.008856 {
        (Z2 / whitePointZr2).powf(1.0/3.0)
    } else {
        7.787 * (Z2 / 86.0) + 16.0 / 116.0
    };

    let fX: f64 = if (X2 / whitePointXr2) > 0.008856 {
        (X2 / whitePointXr2).powf(1.0/3.0)
    } else {
        7.787 * (X2 / 86.0) +16.0 / 116.0
    };

    LAB {
        reconstructedL: 116.0 * fY - 16.0,
        reconstructedA: 500.0 * (fX - fY),
        reconstructedB: 200.0 * (fY - fZ),
    }

}

pub fn grainLABToXYZ(inputLAB: LAB) -> XYZ {
    let mut yComputed: f64 = (inputLAB.reconstructedL + 16.0) / 116.0;
    let mut xComputed: f64 = (inputLAB.reconstructedA / 500.0) + yComputed;
    let mut zComputed: f64 = yComputed - (inputLAB.reconstructedB / 200.0);

    if yComputed.powi(3)  > 0.008856 {
        yComputed = yComputed.powi(3)
    } else {
        yComputed = ( yComputed - 16.0 / 116.0 ) / 7.787
    }

    if xComputed.powi(3)  > 0.008856 {
        xComputed = xComputed.powi(3)
    } else {
        xComputed = (xComputed - 16.0 / 116.0) / 7.787
    }

    if zComputed.powi(3)  > 0.008856 {
        zComputed = zComputed.powi(3)
    } else {
        zComputed = (zComputed - 16.0 / 116.0) / 7.787
    }

    // D65/2°
    // X_2 = 95.047
    // Y_2 = 100.000
    // Z_2 = 108.883
    // D65/10°
    // X_10 = 94.811
    // Y_10 = 100.000
    // Z_10 = 107.304
    // Daylight, sRGB, Adobe-RGB

    let xDaylight: f64 = 95.047;
    let yDaylight: f64 = 100.000;
    let zDaylight: f64 = 108.883;

    XYZ {
        X: xComputed * xDaylight,
        Y: yComputed * yDaylight,
        Z: zComputed * zDaylight,
    }

}

pub fn grainXYZToRGBA(inputXYZ: XYZ) -> RGBA {
    //X, Y and Z input refer to a D65/2° standard illuminant.
    //sR, sG and sB (standard RGB) output range = 0 ÷ 255

    let xValue: f64 = inputXYZ.X / 100.0;
    let yValue: f64 = inputXYZ.Y / 100.0;
    let zValue: f64 = inputXYZ.Z / 100.0;

    let mut redValue: f64 = xValue *  3.2406 + yValue * -1.5372 + zValue * -0.4986;
    let mut greenValue: f64 = xValue * -0.9689 + yValue *  1.8758 + zValue *  0.0415;
    let mut blueValue: f64 = xValue *  0.0557 + yValue * -0.2040 + zValue *  1.0570;

    if redValue > 0.0031308 {
        redValue = 1.055 * redValue.powf(1.0 / 2.4) - 0.055
    } else {
        redValue = 12.92 * redValue
    }

    if greenValue > 0.0031308 {
        greenValue = 1.055 * greenValue.powf(1.0 / 2.4) - 0.055
    } else {
        greenValue = 12.92 * greenValue
    }

    if blueValue > 0.0031308 {
        blueValue = 1.055 * blueValue.powf(1.0 / 2.4) - 0.055
    } else {
        blueValue = 12.92 * blueValue
    }

    RGBA {
        red: (redValue * 255.0) / 256.0,
        green: (greenValue * 255.0) / 256.0,
        blue: (blueValue * 255.0) / 256.0,
        alpha: 1.0,
    }
}