use gtk::{GtkWindowExt, Inhibit, WidgetExt};

use crate::gui::gtkCSS::gtkCSS;
use crate::gui::guestimateABVGUI::guestimateABVGUI;
use crate::gui::increaseABVGUI::increaseABVGUI;
use crate::gui::grainToABVGUI::grainToABVGUI;
use crate::gui::realABVGUI::realABVGUI;
use crate::gui::waterSpargeGUI::waterSpargeGUI;
use crate::gui::guestimateIBUGUI::guestimateIBUGUI;
use crate::gui::gyleCarbonationGUI::gyleCarbonationGUI;
use crate::gui::champagneCarbonationGUI::champagneCarbonationGUI;


pub fn startGTK(application: &gtk::Application) {

    let builder: gtk::Builder = gtk::Builder::new_from_string(include_str!("BrewStillery.glade"));

    let mainWindow: gtk::ApplicationWindow = builder.get_object("mainWindow")
    .expect("startGTK(), mainWindow");

    mainWindow.set_application(application);

    mainWindow.connect_delete_event(clone!(mainWindow => move |_, _| {
        mainWindow.destroy();
        Inhibit(false)
    }));

    gtkCSS(&mainWindow);

    guestimateABVGUI(&builder);

    increaseABVGUI(&builder);

    realABVGUI(&builder);

    grainToABVGUI(&builder);

    waterSpargeGUI(&builder);

    guestimateIBUGUI(&builder);

    gyleCarbonationGUI(&builder);

    champagneCarbonationGUI(&builder);

    mainWindow.show_all();
}