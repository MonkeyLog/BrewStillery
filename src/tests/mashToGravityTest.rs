use crate::functions::commonFunctions::mashToGravity;
use approx::ulps_eq;

#[test]
fn mashToGravityTest() {
    let volumeInGallonsUS: f64 = 20.0;
    let weightInPounds: f64 = 60.0;
    let grainGravity: f64 = 1.037;

    ulps_eq!(mashToGravity(volumeInGallonsUS, weightInPounds, grainGravity), 1.06327);
}