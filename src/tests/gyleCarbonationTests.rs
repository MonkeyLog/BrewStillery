use crate::enums::generalEnums::imperialOrMetric;
use crate::functions::gyleCarbonation::{gyleData, gyleCarbonationMaths, gyleCarbonationFormatting};
use approx::ulps_eq;

#[test]
fn gyleCarbonationMathsImperialGBTest() {
    let mut allInputs: gyleData = gyleData {
        startingGravity: 1.0525962648284455,
        desiredCO2Level: 4.0,
        finalVolume: 90.9218,
        gyleVolumeFloat: 0.0,
        gyleCarbonationUnitsEnum: imperialOrMetric::ImperialGB,
    };

    gyleCarbonationMaths(&mut allInputs);

    ulps_eq!(allInputs.gyleVolumeFloat, 9.855318842315654);
}

#[test]
fn gyleCarbonationMathsImperialUSTest() {
    let mut allInputs: gyleData = gyleData {
        startingGravity: 1.0525962648284455,
        desiredCO2Level: 4.0,
        finalVolume: 75.70823568,
        gyleVolumeFloat: 0.0,
        gyleCarbonationUnitsEnum: imperialOrMetric::ImperialUS,
    };

    gyleCarbonationMaths(&mut allInputs);

    ulps_eq!(allInputs.gyleVolumeFloat, 8.206269581283896);
}

#[test]
fn gyleCarbonationMathsMetricTest() {
    let mut allInputs: gyleData = gyleData {
        startingGravity: 1.0525962648284455,
        desiredCO2Level: 4.0,
        finalVolume: 75.0,
        gyleVolumeFloat: 0.0,
        gyleCarbonationUnitsEnum: imperialOrMetric::Metric,
    };

    gyleCarbonationMaths(&mut allInputs);

    ulps_eq!(allInputs.gyleVolumeFloat, 8.129501540595038);
}

#[test]
fn gyleCarbonationFormattingImperialGBTest() {
    let allInputsOne: gyleData = gyleData {
        startingGravity: 1.0525962648284455,
        desiredCO2Level: 1.85,
        finalVolume: 90.9218,
        gyleVolumeFloat: 4.55808496457099,
        gyleCarbonationUnitsEnum: imperialOrMetric::ImperialGB,
    };

    let outputOne: String = gyleCarbonationFormatting(&allInputsOne);

    assert_eq!(outputOne, "1 gallon");

    let allInputsMultiple: gyleData = gyleData {
        startingGravity: 1.0525962648284455,
        desiredCO2Level: 3.69,
        finalVolume: 90.9218,
        gyleVolumeFloat: 9.09153163203619,
        gyleCarbonationUnitsEnum: imperialOrMetric::ImperialGB,
    };

    let outputMultiple: String = gyleCarbonationFormatting(&allInputsMultiple);

    assert_eq!(outputMultiple, "2 gallons");

    let allInputsDecimal: gyleData = gyleData {
        startingGravity: 1.0525962648284455,
        desiredCO2Level: 4.0,
        finalVolume: 90.9218,
        gyleVolumeFloat: 9.855318842315654,
        gyleCarbonationUnitsEnum: imperialOrMetric::ImperialGB,
    };

    let outputDecimal: String = gyleCarbonationFormatting(&allInputsDecimal);

    assert_eq!(outputDecimal, "2.17 gallons");
}

#[test]
fn gyleCarbonationFormattingImperialUSTest() {
    let allInputsOne: gyleData = gyleData {
        startingGravity: 1.0525962648284455,
        desiredCO2Level: 1.85,
        finalVolume: 75.70823568,
        gyleVolumeFloat: 3.795399681343802,
        gyleCarbonationUnitsEnum: imperialOrMetric::ImperialUS,
    };

    let outputOne: String = gyleCarbonationFormatting(&allInputsOne);

    assert_eq!(outputOne, "1 gallon");

    let allInputsMultiple: gyleData = gyleData {
        startingGravity: 1.0525962648284455,
        desiredCO2Level: 3.69,
        finalVolume: 75.70823568,
        gyleVolumeFloat: 7.570283688734394,
        gyleCarbonationUnitsEnum: imperialOrMetric::ImperialUS,
    };

    let outputMultiple: String = gyleCarbonationFormatting(&allInputsMultiple);

    assert_eq!(outputMultiple, "2 gallons");

    let allInputsDecimal: gyleData = gyleData {
        startingGravity: 1.0525962648284455,
        desiredCO2Level: 4.0,
        finalVolume: 75.70823568,
        gyleVolumeFloat: 8.206269581283896,
        gyleCarbonationUnitsEnum: imperialOrMetric::ImperialUS,
    };

    let outputDecimal: String = gyleCarbonationFormatting(&allInputsDecimal);

    assert_eq!(outputDecimal, "2.17 gallons");
}

#[test]
fn gyleCarbonationFormattingImperialMetricTest() {
    let allInputsOne: gyleData = gyleData {
        startingGravity: 1.0525962648284455,
        desiredCO2Level: 1.0,
        finalVolume: 37.0,
        gyleVolumeFloat: 1.0026385233400545,
        gyleCarbonationUnitsEnum: imperialOrMetric::Metric,
    };

    let outputOne: String = gyleCarbonationFormatting(&allInputsOne);

    assert_eq!(outputOne, "1 litre");

    let allInputsMultiple: gyleData = gyleData {
        startingGravity: 1.0525962648284455,
        desiredCO2Level: 1.99,
        finalVolume: 37.0,
        gyleVolumeFloat: 1.9952506614467087,
        gyleCarbonationUnitsEnum: imperialOrMetric::Metric,
    };

    let outputMultiple: String = gyleCarbonationFormatting(&allInputsMultiple);

    assert_eq!(outputMultiple, "2 litres");

    let allInputsDecimal: gyleData = gyleData {
        startingGravity: 1.0525962648284455,
        desiredCO2Level: 4.0,
        finalVolume: 75.0,
        gyleVolumeFloat: 8.129501540595038,
        gyleCarbonationUnitsEnum: imperialOrMetric::Metric,
    };

    let outputDecimal: String = gyleCarbonationFormatting(&allInputsDecimal);

    assert_eq!(outputDecimal, "8.13 litres");
}