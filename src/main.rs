#![allow(non_snake_case, non_camel_case_types)]

extern crate gtk;
extern crate gdk;
extern crate gio;
extern crate rayon;
extern crate approx;

#[macro_use]
mod macros;
mod gui;
mod functions;
mod light;
mod constants;
mod enums;
mod structs;
#[cfg(test)]
mod tests;

use gio::{ApplicationExt, ApplicationExtManual};
use std::env::args;
use crate::gui::startGTK::startGTK;


fn main() {
    let applicationWindow: gtk::Application = gtk::Application::new("in.monkeylog.BrewStillery",
                                            gio::ApplicationFlags::empty())
                                       .expect("Initialisation failed");

    applicationWindow.connect_startup(move |application| {
        startGTK(application);
    });

    applicationWindow.connect_activate(|_| {});

    applicationWindow.run(&args().collect::<Vec<_>>());
}